package org.dyndns.warenix.reportform;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;


public class MyActivity extends ActionBarActivity {

    private static final String TAG = MyActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new PlaceholderFragment())
                .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String TAG = PlaceholderFragment.class.getSimpleName();

        private EditText mWhatHappen;
        private EditText mWhere;
        private DatePicker mDate;
        private TimePicker mTime;
        private EditText mSource;
        private EditText mContact;

        private static class Form {

            String mWhat;
            String mWhere;
            String mMonth;
            String mDay;
            String mYear;
            String mHour;
            String mMinute;
            String mAMPM;
            String mSource;
            String mContact;

            private boolean isFilled() {
                boolean missing = TextUtils.isEmpty(mWhat) ||
                                  TextUtils.isEmpty(mWhere) || TextUtils.isEmpty(mMonth)
                                  || TextUtils
                    .isEmpty(mDay) || TextUtils.isEmpty(mYear) || TextUtils.isEmpty(mHour)
                                  || TextUtils.isEmpty(mMinute) || TextUtils.isEmpty(mAMPM)
                                  || TextUtils.isEmpty(mSource) ||
                                  TextUtils.isEmpty(mContact);

                return !missing;
            }
        }


        public PlaceholderFragment() {
            setHasOptionsMenu(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container, false);

            mWhatHappen = (EditText) rootView.findViewById(R.id.text_what_happen);
            mWhere = (EditText) rootView.findViewById(R.id.text_where);
            mDate = (DatePicker) rootView.findViewById(R.id.dp_date);
            mTime = (TimePicker) rootView.findViewById(R.id.tp_time);
            mSource = (EditText) rootView.findViewById(R.id.text_source);
            mContact = (EditText) rootView.findViewById(R.id.text_contact);
            return rootView;
        }


        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_submit) {

                onSubmit();
                return true;
            } else if (id == R.id.action_hkverified) {
                // open facebook page
                final String url = "https://www.facebook.com/hkverified";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        private void onSubmit() {

            Form form = makeForm();
            Log.d(TAG, "submit:" + form.isFilled());
            if (form.isFilled()) {
                doSubmitForm(form);
            } else {
                Toast.makeText(getActivity(), getResources()
                    .getString(R.string.toast_invalid_form), Toast.LENGTH_SHORT).show();
            }
        }

        private void doSubmitForm(Form form) {
            new AsyncTask<Form, Void, Void>() {

                private ProgressDialogFragment mProgressDialogFragment;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                    mProgressDialogFragment = new ProgressDialogFragment();
                    mProgressDialogFragment.show(getFragmentManager(), null);
                }

                @Override
                protected Void doInBackground(Form... forms) {
                    Form form = forms[0];

                    // developement test form
//                    final String
//                        url =
//                        "https://docs.google.com/forms/d/1hsYFXpbK44By4xzDKGnCdfpWzFRNgbS9DMVG1cZbYpA/formResponse";
//                    HashMap<String, String> data = new HashMap<String, String>();
//                    data.put("entry.965480888", form.mWhat);
//                    data.put("entry.863960093_month", form.mMonth);
//                    data.put("entry.863960093_day", form.mDay);
//                    data.put("entry.863960093_year", form.mYear);
//                    data.put("entry.863960093_hour", form.mHour);
//                    data.put("entry.863960093_minute", form.mMinute);
//                    data.put("entry.863960093_ampm", form.mAMPM);
//                    data.put("entry.1073671308", form.mWhere);
//                    data.put("entry.595134604", form.mSource);
//                    data.put("entry.1192950443", form.mContact);

                    // live report form
                    final String
                        url =
                        "https://docs.google.com/forms/d/1py0WpFSE2lGDbsDWzi8C9CMHxMqrdpNCj6j5QzyuWzs/formResponse";
                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("entry.197206764", form.mWhat);
                    data.put("entry.543565379_month", form.mMonth);
                    data.put("entry.543565379_day", form.mDay);
                    data.put("entry.543565379_year", form.mYear);
                    data.put("entry.543565379_hour", form.mHour);
                    data.put("entry.543565379_minute", form.mMinute);
                    data.put("entry.543565379_ampm", form.mAMPM);
                    data.put("entry.509974285", form.mWhere);
                    data.put("entry.575074680", form.mSource);
                    data.put("entry.27443865", form.mContact);

                    try {
                        String response = makeFormPost(null, url, data);
                        Log.d(TAG, response);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    mProgressDialogFragment.dismiss();
                    Toast.makeText(getActivity(),
                                   getResources().getString(R.string.toast_submitted),
                                   Toast.LENGTH_LONG).show();
                }
            }.execute(form);
        }

        private Form makeForm() {
            Form form = new Form();
            form.mWhat = mWhatHappen.getText().toString();
            form.mWhere = mWhere.getText().toString();
            form.mSource = mSource.getText().toString();
            form.mContact = mContact.getText().toString();

            form.mHour = String.valueOf(mTime.getCurrentHour());
            form.mMinute = String.valueOf(mTime.getCurrentMinute());
            form.mAMPM = mTime.getCurrentHour() < 12 ? "AM" : "PM";
            form.mYear = String.valueOf(mDate.getYear());
            form.mMonth = String.valueOf(mDate.getMonth());
            form.mDay = String.valueOf(mDate.getDayOfMonth());

            form.mSource = mSource.getText().toString();

            return form;
        }

    }

    public static class ProgressDialogFragment extends DialogFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setCancelable(false);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setMessage(getString(R.string.progress_message));
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);

// Disable the back button
            DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface dialog, int keyCode,
                                     KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        return true;
                    }
                    return false;
                }

            };
            dialog.setOnKeyListener(keyListener);
            return dialog;
        }
    }

    public static String makeFormPost(Context context, String
        url, HashMap<String, String> data)
        throws IOException {
        HttpPost post = new HttpPost(url);
        post.setHeader("Content-Type",
                       "application/x-www-form-urlencoded;charset=UTF-8");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        Set<String> keySet = data.keySet();
        for (String key : keySet) {
            params.add(new BasicNameValuePair(key, data.get(key)));
        }
        post.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
        String entityString = EntityUtils.toString(post.getEntity());
//        Log.d(TAG, entityString);
        HttpClient client = new DefaultHttpClient();
        HttpResponse response = client.execute(post);
        return getContent(response);
    }

    public static String getContent(HttpResponse response) throws IOException {
        BufferedReader rd =
            new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        String body;
        StringBuffer sb = new StringBuffer();
        while ((body = rd.readLine()) != null) {
            sb.append(body);
        }
        return sb.toString();
    }
}
